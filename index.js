/*
	What is a client?

	A client is an application which creates requests for resources from a server. A client will trigger an action, in the web development context, through a URL and wait for the response of a server,

	What is a server?

	A server is able to host and deliver resources requested by a client. In fact, a single server can handle multiple clients.

	What si Node.js?

	Nodejs is a runtime environment which allows us to create / develop backend / derver-side applications with Javascript. Because by default, Javascript was conceptualized solely to the front end.

	Why is NodeJS popular?
 
	Performance - NodeJS is one of the most performing environment for creating backend application with JS.

	Familiarity - Since NodeJS is built and uses JS as its language, it is very familiar for most developers.

	NPM - Node Package Manager is the largest registry for node packages. Packages are bits of programs, methods, functions, codes, that greatly help in the development of an application
*/

//console.log("To God be the Glory!");




// require is a built-in JS method which allows us to import packages. Packages are pieces of code we can integrate into our application.
let http = require("http");

// http is a default package that comes with NodeJS. It allows us to use methods that let us create servers.

// variable http - a module. Modules are packages we imported. 

// Modules are objects that contains codes, methods or data.

// The http module let us create a server which is able to communicate with a client through the use of Hypertext Transfer Protocol.

// (protocol to client-server communication is this URL) http://localhost:4000
console.log(http);

http.createServer(function(request, response){

/*
	createServer() method is a method from http module that allows us to handle requests and responses from a client and a server respectively.

	.createServer() method takes a function argument which is able to receive 2 objects. The request object which contains details of the request from the client. The response object which contains details of the response from the server. The createServer() method ALWAYS receives the request object first before the response.

	response.writeHead() is a method of the response object. It allows us to add headers to our response. Headers are additional information about our response. We have 2 arguments in our writeHead() method. The first is the HTTP status code. An HTTP status is just a numerical cose to let the client know about the status of their request. 200, means OK, 404 means the resource cannot be found. 'Content-Type' is one of the most recognizeable headers. It simply pertains to the data type of our response.

	response.end() it ends our response. It also able to send a message / data as a string.
*/

/*
	Servers can actually respond differently with different requests.
	
	We start our requests with our URL. A client can start a different request with a different URl

	http://locahost:4000/ is not the same with http://localhost:4000/profile
	/ = url endpoint (default)
	/profile = url endpoint

	We can differentiate requests by their endpoints, we should be able to respond differently to different endpoints

	information about the URL endpoint of the request is in the request object

	request.url contains the URL endpoint
	
	/ - default endpoint - request URL: http://localhost:4000/
	/favicon.ico - Browser's default behavior to retrieve the favicon
	/profile - request URL - http://localhost:4000/profile
	/favicon.ico
	

	Different Requests Require Different Responses

	The process or way to respond differently to a request is called a route

*/
	//console.log(request.url)  <=== contains the endpoint

	// Specific response to a specific endpoint
	if(request.url === "/"){

		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Hello from our first Server! This is from / endpoint.");

	} else if (request.url === "/profile"){

		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Hi! I am Squall Leonhart!");

	}


	

/*
	.listen() allows us to assign a port to our server. This will allow us to serve our index.js server in our local machine assigned to port 4000. There are several tasks and processes on our computer / machine that run on different port numbers.
	
	hypertext transfer protocol - http://localhost:4000/ - server

	localhost (local machine or computer)

	4000, 4040, 4200, 8000, 5000, 3000 - ports usually used for web development	

	console.log() is added to show validation which port number our server is running on. In fact, the more complex our servers become, the more things we can check first before running the serevr

*/
}).listen(4000);

console.log("Server is running in localHost:4000!");













